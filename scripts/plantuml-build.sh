apt-get install -qy wget openjdk-8-jdk graphviz

wget -nc http://sourceforge.net/projects/plantuml/files/plantuml.jar/download -O plantuml.jar

SOURCES_FILES=`find plantuml -iname '*.puml'`
OUTPUT_DIR=`pwd`/generated

mkdir -p $OUTPUT_DIR

for file in $SOURCES_FILES; do
    printf "Build $file... "
    DIR=$(dirname "${file}")
    java -jar plantuml.jar -charset UTF-8 -progress -o $OUTPUT_DIR/$DIR $file
    printf " DONE!\n"
done
